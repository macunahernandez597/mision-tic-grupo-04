function eliminarProducto(parametro){
    const apiObtenerProductos = "http://localhost:8094/back03/producto/borrar/" + parametro;
    const miPromesaProductos = fetch(apiObtenerProductos,{method:'DELETE'}).then((respuesta) =>respuesta.json()) // el primer then es solo para recibir la info
    .catch(miError => console.log(miError))
      
    Promise.all([miPromesaProductos]).then((arregloDatos) => { // ejecuta todas las promesas
      const datos = arregloDatos[0];
      if (datos.status == "200") {
        document.getElementById('alertProducEliminar').classList.add('alert-primary');
        document.getElementById('msgProducEliminar').innerHTML="El Producto ha sido eliminado😁";
      } else {
        document.getElementById('alertProducEliminar').classList.add('alert-danger');
        document.getElementById('msgProducEliminar').innerHTML="El Producto NO ha sido eliminado 😕";
      }
    });
}