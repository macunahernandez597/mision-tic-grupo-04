function obtenerTodosProductosAdmin() {
    const apiObtenerProductos = "http://localhost:8094/back03/producto/product";
    const miPromesaProductos = fetch(apiObtenerProductos).then((respuesta) =>respuesta.json()); // el primer then es solo para recibir la info
  
    Promise.all([miPromesaProductos]).then((arregloDatos) => { // ejecuta todas las promesas
      const datos = arregloDatos[0];
      crearFilaProductosAdmin(datos);
      console.log(datos);
    });
  }
  
  function crearFilaProductosAdmin(arregloObjeto) { // imprimir todo lo que llegue
    const cantidad = arregloObjeto.length;
    for (let i = 0; i < cantidad; i++) {
      const codProducto = arregloObjeto[i].codProducto;
      const nomProducto = arregloObjeto[i].nombreProducto;
      const valProducto = arregloObjeto[i].valorProducto;
  
      document.getElementById("tablaProducAdmin").insertRow(-1).innerHTML = "<td>" + codProducto + "</td>" 
        + "<td>" + nomProducto + "</td>" 
        + "<td>" + valProducto + "</td>"
        + "<a href='javascript:confirmarProduEliminar("
        + codProducto + ");'><i class='fa-solid fa-trash check-rojo'></i></a> &nbsp;"
        + "<a href='#producupdate/"+codProducto+"'> <i class='fa-solid fa-pen-to-square'></i></a>"
        +"<td/>";

      // el "tablaProducAdmin" se saca de produclistado
    }
  }

  function confirmarProduEliminar(codigo){
    if(window.confirm("¿Estas seguro que quieres eliminar el Producto?😮")){
      window.location.replace("#producdelete/" + codigo);
    }
  }
  