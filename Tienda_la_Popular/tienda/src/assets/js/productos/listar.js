function obtenerTodosProductos() {
    const apiObtenerProductos = "http://localhost:8094/back03/producto/product";
    const miPromesaProductos = fetch(apiObtenerProductos).then((respuesta) =>respuesta.json()); // el primer then es solo para recibir la info
  
    Promise.all([miPromesaProductos]).then((arregloDatos) => { // ejecuta todas las promesas
      const datos = arregloDatos[0];
      crearFilaProdutos(datos);
      console.log(datos);
    });
  }
  
  function crearFilaProdutos(arregloObjeto) { // imprimir todo lo que llegue
    const cantidad = arregloObjeto.length;
    for (let i = 0; i < cantidad; i++) {
      const codProduc = arregloObjeto[i].codProducto;
      const nomProduc = arregloObjeto[i].nombreProducto;
      const valProduc = arregloObjeto[i].valorProducto;

  
      document.getElementById("tablaProductos").insertRow(-1).innerHTML =
        "<td>" + codProduc + "</td>" + "<td>" + nomProduc + "</td>" + "<td>" + valProduc + "</td>";
      // el "tablaRoles" se saca de rollistado
    }
  }
  