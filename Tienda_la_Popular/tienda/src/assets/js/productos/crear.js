function crearProduct(){

    const nombre = document.getElementById("nom").value;
    const valor = document.getElementById("val").value;
    if(nombre !== ""){
        let objetoEnviar = {
            nombreProducto: nombre,
            valorProducto: valor
        }

        const apiCrear = "http://localhost:8094/back03/producto/crear";
        fetch(apiCrear,{
            method:"POST",
            body:JSON.stringify(objetoEnviar),
            headers:{"Content-type":"application/json; charset=UTF-8"}
        })
        .then(respuesta=>respuesta.json())
        .then(datos=>{
            if (datos.hasOwnProperty("codProducto")) {
                document.getElementById("producMsgOk").style.display = "";
                document.getElementById("producMsgError").style.display = "none";
            } else {
                document.getElementById("producMsgOk").style.display = "none";
                document.getElementById("producMsgError").style.display = "";
            }
        }
        )
        .catch(
            miError=> console.log(miError)
        );
        document.getElementById("formProduc").reset();
        document.getElementById("formProduc").classList.remove("was-validated");
           
    }
}