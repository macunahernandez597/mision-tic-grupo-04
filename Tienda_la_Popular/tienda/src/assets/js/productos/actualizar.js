function actualizarProdu(){
    const codigo = document.getElementById("cod").value;
    const nombre = document.getElementById("nom").value;
    const valor = document.getElementById("val").value;
    
        let objetoEnviar = {
            codProducto: codigo,
            nombreProducto: nombre,
            valorProducto: valor
        }

        const apiCrear = "http://localhost:8094/back03/producto/actualizar/";
        fetch(apiCrear,{
            method:"PUT",
            body:JSON.stringify(objetoEnviar),
            headers:{"Content-type":"application/json; charset=UTF-8"}
        })
        .then(respuesta=>respuesta.json())
        .then(datos=>{
            if (datos.hasOwnProperty("codProducto")) {
                document.getElementById("producMsgOk").style.display = "";
                document.getElementById("producMsgError").style.display = "none";
            } else {
                document.getElementById("producMsgOk").style.display = "none";
                document.getElementById("producMsgError").style.display = "";
            }
            window.location.replace("#producmanage");
        }
        )
        .catch(
            miError=> console.log(miError)
        );       
    }
