function mostrarProductos(codigo) {
    const apiObtenerProductos = "http://localhost:8094/back03/producto/uno/" + codigo;
    const miPromesaProductos = fetch(apiObtenerProductos)
    .then((respuesta) =>respuesta.json())    // el primer then es solo para recibir la info
    .then((dato)=>{
        if (dato.hasOwnProperty("codProducto")) {
            document.getElementById("cod").value = dato.codProducto;
            document.getElementById("nom").value = dato.nombreProducto;
            document.getElementById("val").value = dato.valorProducto;
        } else {
            
        }
    })
    .catch((miError) => console.log(miError));
   
  
  }