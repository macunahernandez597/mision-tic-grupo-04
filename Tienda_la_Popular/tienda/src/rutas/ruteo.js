'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('home', 'inicio.html', true),
            new Route('produclist', 'productos/produclistado.html'),
            new Route('producadd', 'productos/produccrear.html'),
            new Route('producmanage', 'productos/producadmin.html'),
            new Route('producdelete', 'productos/produceliminar.html'),
            new Route('producupdate', 'productos/producactualizar.html'),
            new Route('login', 'acceso.html')
        ]);
    }
    init();
}());
