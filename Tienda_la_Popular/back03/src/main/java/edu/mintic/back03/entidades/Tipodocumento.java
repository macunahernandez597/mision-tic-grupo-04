package edu.mintic.back03.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tiposdocumentos")
public class Tipodocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_tipodocumento")
    private Integer codTipodocumento;

    @Column(name = "nombre_tipodocumento")
    private String nombreTipodocumento;
    
    public Tipodocumento() {
    }

    public Tipodocumento(Integer codTipodocumento) {
        this.codTipodocumento = codTipodocumento;
    }

    public Tipodocumento(Integer codTipodocumento, String nombreTipodocumento) {
        this.codTipodocumento = codTipodocumento;
        this.nombreTipodocumento = nombreTipodocumento;
    }

    public Integer getCodTipodocumento() {
        return codTipodocumento;
    }

    public void setCodTipodocumento(Integer codTipodocumento) {
        this.codTipodocumento = codTipodocumento;
    }

    public String getNombreTipodocumento() {
        return nombreTipodocumento;
    }

    public void setNombreTipodocumento(String nombreTipodocumento) {
        this.nombreTipodocumento = nombreTipodocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipodocumento != null ? codTipodocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipodocumento)) {
            return false;
        }
        Tipodocumento other = (Tipodocumento) object;
        if ((this.codTipodocumento == null && other.codTipodocumento != null) || (this.codTipodocumento != null && !this.codTipodocumento.equals(other.codTipodocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.mintic.back03.entidades.Tiposdocumentos[ codTipodocumento=" + codTipodocumento + " ]";
    }

}
