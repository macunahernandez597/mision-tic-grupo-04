
package edu.mintic.back03.controladores;

import edu.mintic.back03.entidades.Producto;
import edu.mintic.back03.servicios.ProductosServicios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/producto")
@CrossOrigin(origins = "*")
public class ProductosControlador {
    
    @Autowired
    private ProductosServicios producService;
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public List<Producto> demeProducto(){
        return producService.consultar();
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/crear", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Producto> creaProducto(@RequestBody Producto objprod){
        if (producService.agregar(objprod)) {
            return ResponseEntity.ok(objprod);
        } else {
            return ResponseEntity.notFound().build();
        }   
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto eliminado correctamente")
    @RequestMapping(value = "/borrar/{codigo}", method = RequestMethod.DELETE)
    public void borreloProducto(@PathVariable Integer codigo) {
        producService.eliminar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/uno/{codigo}", method = RequestMethod.GET)
    public Producto obtenerUno(@PathVariable int codigo) {
        return producService.buscar(codigo);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/buscar/{codigo}", method = RequestMethod.GET)
    public Producto buscarUno (@PathVariable Integer codigo) {
        return producService.buscar(codigo);
    }
    
    @ResponseStatus(code = HttpStatus.OK, reason = "Objeto actualizado correctamente")
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
    public Boolean actuProducto(@RequestBody Producto miObjeto) {
        return producService.actualizar(miObjeto);
    }
    
    
}
