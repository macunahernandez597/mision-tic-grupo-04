package edu.mintic.back03.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "documentos")
public class Documento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_documento")
    private Integer codDocumento;

    @Column(name = "fecha_documento")
    @Temporal(TemporalType.DATE)
    private Date fechaDocumento;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_documento")
    private BigDecimal valorDocumento;

    @JoinColumn(name = "cod_tipodocumento", referencedColumnName = "cod_tipodocumento")
    @ManyToOne(optional = false)
    private Tipodocumento codTipodocumento;

    public Documento() {
    }

    public Documento(Integer codDocumento) {
        this.codDocumento = codDocumento;
    }

    public Documento(Integer codDocumento, Date fechaDocumento, BigDecimal valorDocumento) {
        this.codDocumento = codDocumento;
        this.fechaDocumento = fechaDocumento;
        this.valorDocumento = valorDocumento;
    }

    public Integer getCodDocumento() {
        return codDocumento;
    }

    public void setCodDocumento(Integer codDocumento) {
        this.codDocumento = codDocumento;
    }

    public Date getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(Date fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public BigDecimal getValorDocumento() {
        return valorDocumento;
    }

    public void setValorDocumento(BigDecimal valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public Tipodocumento getCodTipodocumento() {
        return codTipodocumento;
    }

    public void setCodTipodocumento(Tipodocumento codTipodocumento) {
        this.codTipodocumento = codTipodocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codDocumento != null ? codDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.codDocumento == null && other.codDocumento != null) || (this.codDocumento != null && !this.codDocumento.equals(other.codDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.mintic.back03.entidades.Documentos[ codDocumento=" + codDocumento + " ]";
    }

}
