
package edu.mintic.back03.repositorios;

import edu.mintic.back03.entidades.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductosRepositorio extends JpaRepository<Producto, Integer>{
    
    @Query("SELECT pr FROM Producto pr")
    public List<Producto> obtenerProducto();
}
