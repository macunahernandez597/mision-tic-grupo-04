package edu.mintic.back03.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventarios")
public class Inventario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "cod_producto")
    private Integer codProducto;

    @Column(name = "cantidad_inventario")
    private int cantidadInventario;

    @Column(name = "cantidadmaxima_inventario")
    private int cantidadmaximaInventario;

    @Column(name = "cantidadminima_inventario")
    private int cantidadminimaInventario;

    @JoinColumn(name = "cod_producto", referencedColumnName = "cod_producto", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Producto productos;

    public Inventario() {
    }

    public Inventario(Integer codProducto) {
        this.codProducto = codProducto;
    }

    public Inventario(Integer codProducto, int cantidadInventario, int cantidadmaximaInventario, int cantidadminimaInventario) {
        this.codProducto = codProducto;
        this.cantidadInventario = cantidadInventario;
        this.cantidadmaximaInventario = cantidadmaximaInventario;
        this.cantidadminimaInventario = cantidadminimaInventario;
    }

    public Integer getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(Integer codProducto) {
        this.codProducto = codProducto;
    }

    public int getCantidadInventario() {
        return cantidadInventario;
    }

    public void setCantidadInventario(int cantidadInventario) {
        this.cantidadInventario = cantidadInventario;
    }

    public int getCantidadmaximaInventario() {
        return cantidadmaximaInventario;
    }

    public void setCantidadmaximaInventario(int cantidadmaximaInventario) {
        this.cantidadmaximaInventario = cantidadmaximaInventario;
    }

    public int getCantidadminimaInventario() {
        return cantidadminimaInventario;
    }

    public void setCantidadminimaInventario(int cantidadminimaInventario) {
        this.cantidadminimaInventario = cantidadminimaInventario;
    }

    public Producto getProductos() {
        return productos;
    }

    public void setProductos(Producto productos) {
        this.productos = productos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codProducto != null ? codProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.codProducto == null && other.codProducto != null) || (this.codProducto != null && !this.codProducto.equals(other.codProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.mintic.back03.entidades.Inventarios[ codProducto=" + codProducto + " ]";
    }

}
