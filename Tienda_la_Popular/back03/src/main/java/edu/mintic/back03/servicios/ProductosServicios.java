
package edu.mintic.back03.servicios;

import edu.mintic.back03.entidades.Producto;
import edu.mintic.back03.interfaces.Operaciones;
import edu.mintic.back03.repositorios.ProductosRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service("producService")
public class ProductosServicios implements Operaciones<Producto>{

    @Autowired
    private ProductosRepositorio producRepo;
    
    @Override
    public List<Producto> consultar() {
        return producRepo.obtenerProducto();
    }

    @Override
    public Boolean agregar(Producto miObjeto) {
       Producto temporal = producRepo.save(miObjeto);
       return temporal != null;
    }

    @Override
    public Boolean eliminar(Integer llavePrimaria) {
        producRepo.deleteById(llavePrimaria);
        return !producRepo.existsById(llavePrimaria); // esto es igual a if  
    }
    
    @Override
    public Producto buscar(Integer llavePrimaria) {
        return producRepo.findById(llavePrimaria).get();
    }

    @Override
    public Boolean actualizar(Producto miObjeto) {
       Optional<Producto> objVerificar = producRepo.findById(miObjeto.getCodProducto());
        if (objVerificar.isPresent()) {        // la diferencia aca es que le enviamos el codigo
            producRepo.save(miObjeto);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Objeto no aceptado");
        }
    }
    
    @Override
    public Integer cantidadResgistros() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
  
}
