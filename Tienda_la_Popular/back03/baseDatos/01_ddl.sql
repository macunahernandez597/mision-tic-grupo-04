/*==============================================================*/
/* Table: datalle                                               */
/*==============================================================*/
create table detalles
(
   cod_detalle          int not null auto_increment  comment '',
   cod_producto         int not null  comment '',
   cod_documento        int not null  comment '',
   primary key (cod_detalle)
);

/*==============================================================*/
/* Table: documentos                                            */
/*==============================================================*/
create table documentos
(
   cod_documento        int not null auto_increment  comment '',
   cod_tipodocumento    int not null  comment '',
   fecha_documento      date not null  comment '',
   valor_documento      numeric(10,2) not null  comment '',
   primary key (cod_documento)
);

/*==============================================================*/
/* Table: inventario                                            */
/*==============================================================*/
create table inventarios
(
   cod_producto int not null  comment '',
   cantidad_inventario  int not null  comment '',
   cantidadmaxima_inventario int not null  comment '',
   cantidadminima_inventario int not null  comment '',
   primary key (cod_producto)
);

/*==============================================================*/
/* Table: producticos                                            */
/*==============================================================*/
create table productos
(
   cod_producto         int not null auto_increment  comment '',
   nombre_producto      varchar(200) not null  comment '',
   valor_producto       numeric(10,2) not null  comment '',
   primary key (cod_producto)
);

/*==============================================================*/
/* Table: tiposdocumentos                                       */
/*==============================================================*/
create table tiposdocumentos
(
   cod_tipodocumento    int not null auto_increment  comment '',
   nombre_tipodocumento varchar(100) not null  comment '',
   primary key (cod_tipodocumento)
);


alter table detalles add constraint fk_detalle_reference_document foreign key (cod_documento)
      references documentos (cod_documento) on delete restrict on update cascade;

alter table documentos add constraint fk_document_reference_tiposdoc foreign key (cod_tipodocumento)
      references tiposdocumentos (cod_tipodocumento) on delete restrict on update cascade;

alter table inventarios add constraint fk_inventar_reference_producto foreign key (cod_producto) 
references productos(cod_producto) on delete restrict on update cascade;

alter table detalles add constraint fk_detalle_reference_producto foreign key (cod_producto)
      references productos(cod_producto) on delete restrict on update cascade;

	  
	  
	  

