INSERT INTO productos(cod_producto, nombre_producto, valor_producto) VALUES (1, 'Arroz Diana', 2200);
INSERT INTO productos(cod_producto, nombre_producto, valor_producto) VALUES (2, 'Panela Mogotes', 2100);
INSERT INTO productos(cod_producto, nombre_producto, valor_producto) VALUES (3, 'Aceite Oleocali', 10000);
INSERT INTO productos(cod_producto, nombre_producto, valor_producto) VALUES (4, 'Pasta Doria', 1200);


INSERT INTO inventarios(cod_producto, cantidad_inventario, cantidadmaxima_inventario, cantidadminima_inventario) VALUES (1, 34, 40, 10);
INSERT INTO inventarios(cod_producto, cantidad_inventario, cantidadmaxima_inventario, cantidadminima_inventario) VALUES (2, 45, 55, 12);
INSERT INTO inventarios(cod_producto, cantidad_inventario, cantidadmaxima_inventario, cantidadminima_inventario) VALUES (3, 100, 500, 50);
INSERT INTO inventarios(cod_producto, cantidad_inventario, cantidadmaxima_inventario, cantidadminima_inventario) VALUES (4, 3, 30, 8);


INSERT INTO tiposdocumentos(cod_tipodocumento, nombre_tipodocumento) VALUES ( 1, 'Compra Nutresa');
INSERT INTO tiposdocumentos(cod_tipodocumento, nombre_tipodocumento) VALUES ( 2, 'Venta 01' );
INSERT INTO tiposdocumentos(cod_tipodocumento, nombre_tipodocumento) VALUES ( 3, 'Compra Postobom');
INSERT INTO tiposdocumentos(cod_tipodocumento, nombre_tipodocumento) VALUES ( 4, 'Compra Colcafe');


INSERT INTO documentos(cod_documento, cod_tipodocumento, fecha_documento, valor_documento) VALUES (1, 1, 6/09/2022, 2500);
INSERT INTO documentos(cod_documento, cod_tipodocumento, fecha_documento, valor_documento) VALUES (2, 2, 4/09/2022, 4500);
INSERT INTO documentos(cod_documento, cod_tipodocumento, fecha_documento, valor_documento) VALUES (3, 3, 6/05/2022, 2689.5);
INSERT INTO documentos(cod_documento, cod_tipodocumento, fecha_documento, valor_documento) VALUES (4, 4, 30/09/2022, 34500);



INSERT INTO detalles(cod_detalle, cod_producto, cod_documento) VALUES (1, 1, 1);
INSERT INTO detalles(cod_detalle, cod_producto, cod_documento) VALUES (2, 2, 2);
INSERT INTO detalles(cod_detalle, cod_producto, cod_documento) VALUES (3, 3, 3);
INSERT INTO detalles(cod_detalle, cod_producto, cod_documento) VALUES (4, 4, 4);


